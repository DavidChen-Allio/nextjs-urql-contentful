# NextJs + Urql + Contentful

-   This project starter uses nextjs (v12.1.0) and urql (a lightweight graphql client) assuming Contentful as the core headless cms.

## Setup

-   see [setup.md](./docs/setup.md) for how this repo was set up

## Provision

-   see [provision.md](./docs/provision.md) for provisioning steps

## Development

-   see [development.md](./docs/development.md) for development flow
